# DREAMHACK #

Work on DeepDream

If deepdream works this should

## USAGE

python dream.py -i inputimage.jpg -m CUB

--gpu this is the gpu id number. If you have multiple GPU's you probably already know how to do this. If you have your single GPU Compiled it's 0. If you DONT have a GPU then this will take forever, but the correct flag is -1. Default 0

-pre string you want prepended to the output picture names. Default dhout

--iter How many iterations to run. Default: 100

-m is the model flag. The available flags are:

### IMG

This is the imagenet one that comes pre-bundled. Puppyslug galore.

## 256

http://www.vision.caltech.edu/Image_Datasets/Caltech256/

Seems to be a lot of bowling pins. Maybe birds?

## EUC

http://foodcam.mobi/dataset100.html

Food dataset. Kinda grostesque looking, makes people look zombie-ish and creepy. It's called the UEC dataset, but some drunk grad student had it imaged on our machine as EUC, and the name stuck.

## CUB

http://www.vision.caltech.edu/visipedia/CUB-200-2011.html

200 bird dataset. Makes birds. Some look like dinosaurs.